# GeoPagosChallenge

## <a name="title"></a> Android Test - Geopagos

Using MercadoPago’s Public API (https://www.mercadopago.com.ar/developers/es/) we need you to create a Payment’s App.

We will guide you through the URIs you will need to use for this exercise, the parameters should be included as query params.

## <a name="payment-methods"></a> Payment Methods:
https://api.mercadopago.com/v1/payment_methods

**Parameters:**

public_key

## <a name="card-issuers"></a> Card Issuers:
https://api.mercadopago.com/v1/payment_methods/card_issuers

**Parameters:**

public_key

payment_method_id

## <a name="installments"></a> Installments:
https://api.mercadopago.com/v1/payment_methods/installments

**Parameters:**

public_key

amount

payment_method_id

issuer.id

## <a name="requirements"></a> Requirements:
You should register on MercadoPago Developer site for a public key, also, there’s no need for you to use the MercadoPago SDK, you will need only the information we gave you.

You can write it in Kotlin or Java.

The app should have a very specific navigation flow:

1.  Amount Input View
2.  Payment Methods List
3.  Banks List
4.  Installments List
5.  Success View

We need in the first step to give the proper treatment to the Amount (Should be formatted as you type, and also should show a Currency Symbol).

You should consider that some parameters are going to be part of the response as you go forward in the navigation flow.

In the last part of the flow, you should list all the options that were selected in the process (including the entered amount), and should return to the first view of the navigation flow without any previous data.

## <a name="Considerations"></a> Considerations:
*  Android Architecture Patterns
*  Object Modeling
*  Design Patterns
*  Requests Methods
*  Navigation Flow

**We prefer:**
*  MVVM+data binding
*  Constraint layout

Also, we will be considering as a plus anything extra you do.

You can send the compressed source code and APK by mail for us to evaluate.