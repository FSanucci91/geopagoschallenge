package com.ar.geopagos.ui.payment

import com.ar.geopagos.model.ModelPaymentMethod

interface PaymentView {
    fun getAmountValue(): Double

    fun toggleProgressBar(active: Boolean)

    fun showPaymentMethodsData(payMethodsList: List<ModelPaymentMethod>)

    fun showMaxLimitOverdueError()

    fun showDeprecatedError()

    fun showServiceError()

    fun showEmptyDataResponse()

    fun showNetworkError()

    fun verifyAvailableNetwork(): Boolean

    fun updateDataList(data: List<ModelPaymentMethod>)

    fun startBankActivity(payMethod: ModelPaymentMethod, amount: Double)
}