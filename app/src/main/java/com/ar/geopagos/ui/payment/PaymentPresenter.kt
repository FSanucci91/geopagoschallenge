package com.ar.geopagos.ui.payment

import com.ar.geopagos.core.BasePresenter
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.paymentMethods.IPaymentMethodsListener
import com.ar.geopagos.network.paymentMethods.PaymentMethodsAdapter

class PaymentPresenter(
    override val view: PaymentView
) : BasePresenter<PaymentView> {

    private var amount: Double = 0.0
    var adapter = PaymentMethodsAdapter()
    private lateinit var payMethodSelected: ModelPaymentMethod

    override fun onAttachView() {
        view.toggleProgressBar(true)
        if (view.verifyAvailableNetwork()) {
            adapter.paymentMethodsService(object : IPaymentMethodsListener {
                override fun onSuccessResponse(data: List<ModelPaymentMethod>) {

                    amount = view.getAmountValue()
                    val dataFiltered = data.filter { it.maxValue >= amount }
                    view.run {
                        if (dataFiltered.isEmpty()) {
                            showMaxLimitOverdueError()
                        } else {
                            showPaymentMethodsData(dataFiltered)
                        }
                        toggleProgressBar(false)
                    }
                }

                override fun onEmptyResponse() {
                    view.run {
                        showEmptyDataResponse()
                        toggleProgressBar(false)
                    }
                }

                override fun onDeprecatedPayMethodsResponse() {
                    view.run {
                        showDeprecatedError()
                        toggleProgressBar(false)
                    }
                }

                override fun onErrorResponse() {
                    view.run {
                        showServiceError()
                        toggleProgressBar(false)
                    }
                }
            })
        } else {
            view.showNetworkError()
        }
    }

    fun onItemClicked(position: Int, paymentMethods: MutableList<ModelPaymentMethod>) {
        paymentMethods.forEach { item ->
            item.selected = false
        }
        with(paymentMethods[position]) {
            selected = true
            payMethodSelected = this
        }
        view.updateDataList(paymentMethods)
    }

    fun onConfirmButtonClicked() {
        with(view) {
            startBankActivity(payMethodSelected, getAmountValue())
        }
    }
}