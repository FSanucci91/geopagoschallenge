package com.ar.geopagos.ui.payment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseActivity
import com.ar.geopagos.ui.amount.AmountActivity

class PaymentActivity : BaseActivity() {

    override fun layout(): Int = R.layout.activity_base

    override fun init() {
        val arguments = Bundle().apply {
            putDouble(AMOUNT_KEY, intent.getDoubleExtra(AMOUNT_KEY, 0.0))
        }
        val fragment : PaymentFragment  = PaymentFragment.newInstance(baseContext, arguments)
        replaceFragment(R.id.vActivityBaseContent, fragment)
    }

    override fun onBackPressed() {
        AmountActivity.start(baseContext)
        finish()
    }

    companion object {
        const val AMOUNT_KEY = "amount_value"

        fun start(context: Context, amount: Double) {
            val starter = Intent(context, PaymentActivity::class.java).apply {
                putExtra(AMOUNT_KEY, amount)
            }
            context.startActivity(starter)
        }
    }
}