package com.ar.geopagos.ui.installments

import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPayerCost
import com.ar.geopagos.model.ModelPaymentMethod

interface InstallmentsView {
    fun getAmountValue(): Double

    fun getPayMethod(): ModelPaymentMethod

    fun getCardIssuer(): ModelCardIssuer

    fun toggleProgressBar(active: Boolean)

    fun showInstallmentsData(data: List<ModelPayerCost>)

    fun showEmptyDataResponse()

    fun showServiceError()

    fun showNetworkError()

    fun verifyAvailableNetwork(): Boolean

    fun updateDataList(data: List<ModelPayerCost>)

    fun startConfirmationActivity(
        installment: ModelPayerCost,
        cardIssuer: ModelCardIssuer,
        payMethod: ModelPaymentMethod,
        amount: Double
    )
}