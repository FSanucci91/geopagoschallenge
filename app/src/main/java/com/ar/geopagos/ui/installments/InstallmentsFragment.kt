package com.ar.geopagos.ui.installments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseFragment
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPayerCost
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.banks.BanksActivity
import com.ar.geopagos.ui.success.SuccessActivity
import com.ar.geopagos.utils.adapter.GenericAdapter
import com.ar.geopagos.utils.adapter.GenericViewHolderFactory
import com.ar.geopagos.utils.adapter.IGenericListener
import kotlinx.android.synthetic.main.fragment_installments.vConfirmBtn
import kotlinx.android.synthetic.main.fragment_installments.vRecyclerView
import kotlinx.android.synthetic.main.fragment_payment_methods.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class InstallmentsFragment : BaseFragment<InstallmentsPresenter>(), InstallmentsView {

    override val presenter: InstallmentsPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_installments

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod
    private lateinit var cardIssuer: ModelCardIssuer
    private var installmentList = mutableListOf<ModelPayerCost>()
    private lateinit var listAdapter: GenericAdapter<Any>

    override fun init() {
        amount = arguments!!.getDouble(InstallmentsActivity.AMOUNT_KEY, 0.0)
        payMethod =
            arguments!!.getSerializable(InstallmentsActivity.PAY_METHOD) as ModelPaymentMethod
        cardIssuer =
            arguments!!.getSerializable(InstallmentsActivity.CARD_ISSUER) as ModelCardIssuer

        setRecyclerViewData()
    }

    private fun setRecyclerViewData() {

        listAdapter = object : GenericAdapter<Any>() {
            override fun getLayoutId(position: Int, obj: Any): Int = when (obj) {
                is ModelPaymentMethod -> R.layout.item_card_paymethod
                is ModelCardIssuer -> R.layout.item_card_bank
                else -> R.layout.item_card_installment
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder =
                GenericViewHolderFactory.create(view, viewType, object : IGenericListener {
                    override fun onItemClick(position: Int) {
                        presenter.onItemClicked(position, installmentList)
                    }
                })
        }

        vRecyclerView.run {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter = listAdapter
        }

        LinearSnapHelper().apply { attachToRecyclerView(vRecyclerView) }

        val cardMargin: Int = resources.getDimension(R.dimen.space_min).toInt()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        vRecyclerView.addItemDecoration(ItemDecoration(cardMargin, displayMetrics.widthPixels))
    }

    override fun setListeners() {
        vErrorButton.setOnClickListener {
            backAction()
        }

        vConfirmBtn.setOnClickListener {
            presenter.onConfirmButtonClicked()
        }
    }

    override fun getAmountValue(): Double = amount

    override fun getPayMethod(): ModelPaymentMethod = payMethod

    override fun getCardIssuer(): ModelCardIssuer = cardIssuer

    override fun toggleProgressBar(active: Boolean) {
        vProgressBar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun showInstallmentsData(data: List<ModelPayerCost>) {
        installmentList.apply {
            clear()
            addAll(data)
        }
        listAdapter.setItems(installmentList)
        toggleRecyclerViewData(true)
    }

    override fun showEmptyDataResponse() {
        vSubtitleError.text = getText(R.string.service_empty_data_error)
        toggleRecyclerViewData(false)
    }

    override fun showServiceError() {
        vSubtitleError.text = getText(R.string.service_service_error)
        toggleRecyclerViewData(false)
    }

    override fun showNetworkError() {
        vSubtitleError.text = getText(R.string.service_network_error)
        toggleRecyclerViewData(false)
    }

    override fun verifyAvailableNetwork(): Boolean {
        val connectivityManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun updateDataList(data: List<ModelPayerCost>) {
        listAdapter.setItems(installmentList)
        vConfirmBtn.isEnabled = true
    }

    override fun startConfirmationActivity(
        installment: ModelPayerCost,
        cardIssuer: ModelCardIssuer,
        payMethod: ModelPaymentMethod,
        amount: Double
    ) {
        SuccessActivity.start(requireContext(), amount, payMethod, cardIssuer, installment)
        requireActivity().finish()
    }

    private fun toggleRecyclerViewData(active: Boolean) {
        vRecyclerView.visibility = if (active) View.VISIBLE else View.GONE
        vTitle.visibility = if (active) View.VISIBLE else View.GONE
        vConfirmBtn.visibility = if (active) View.VISIBLE else View.GONE
        vErrorContainer.visibility = if (active) View.GONE else View.VISIBLE
    }

    private fun backAction() {
        BanksActivity.start(requireContext(), amount, payMethod)
        requireActivity().finish()
    }

    companion object {
        fun newInstance(context: Context, arguments: Bundle?) =
            instantiate(
                context,
                InstallmentsFragment::class.java.name,
                arguments
            ) as InstallmentsFragment
    }

}