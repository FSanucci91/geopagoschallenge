package com.ar.geopagos.ui.banks

import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod

interface BanksView {
    fun getAmountValue(): Double

    fun getPayMethod(): ModelPaymentMethod

    fun toggleProgressBar(active: Boolean)

    fun showCardIssuersData(data: List<ModelCardIssuer>)

    fun showEmptyDataResponse()

    fun showServiceError()

    fun showNetworkError()

    fun verifyAvailableNetwork(): Boolean

    fun updateDataList(data: List<ModelCardIssuer>)

    fun startInstallmentsActivity(
        cardIssuer: ModelCardIssuer,
        payMethod: ModelPaymentMethod,
        amount: Double
    )
}