package com.ar.geopagos.ui.banks

import com.ar.geopagos.core.BasePresenter
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.cardIssuer.CardIssuersAdapter
import com.ar.geopagos.network.cardIssuer.ICardIssuersListener

class BanksPresenter(
    override val view: BanksView
) : BasePresenter<BanksView> {

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod
    private lateinit var cardIssuerSelected: ModelCardIssuer
    var adapter = CardIssuersAdapter()

    override fun onAttachView() {
        amount = view.getAmountValue()
        payMethod = view.getPayMethod()

        view.toggleProgressBar(true)
        if (view.verifyAvailableNetwork()) {
            adapter.cardIssuersService(payMethod.id, object : ICardIssuersListener {
                override fun onSuccessResponse(data: List<ModelCardIssuer>) {
                    view.run {
                        showCardIssuersData(data)
                        toggleProgressBar(false)
                    }
                }

                override fun onEmptyResponse() {
                    view.run {
                        showEmptyDataResponse()
                        toggleProgressBar(false)
                    }
                }

                override fun onErrorResponse() {
                    view.run {
                        showServiceError()
                        toggleProgressBar(false)
                    }
                }
            })
        } else {
            view.showNetworkError()
        }
    }

    fun onItemClicked(position: Int, cardIssuers: MutableList<ModelCardIssuer>) {
        cardIssuers.forEach { item ->
            item.selected = false
        }
        with(cardIssuers[position]) {
            selected = true
            cardIssuerSelected = this
        }
        view.updateDataList(cardIssuers)
    }

    fun onConfirmButtonClicked() {
        with(view) {
            startInstallmentsActivity(cardIssuerSelected, payMethod, amount)
        }
    }
}