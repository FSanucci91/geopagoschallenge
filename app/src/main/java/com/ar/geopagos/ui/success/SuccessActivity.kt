package com.ar.geopagos.ui.success

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseActivity
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPayerCost
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.amount.AmountActivity
import com.ar.geopagos.ui.installments.InstallmentsActivity
import kotlinx.android.synthetic.main.activity_success.*
import kotlinx.android.synthetic.main.item_success.view.*

class SuccessActivity : BaseActivity() {

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod
    private lateinit var cardIssuer: ModelCardIssuer
    private lateinit var installment: ModelPayerCost

    override fun layout(): Int = R.layout.activity_success

    override fun init() {
        amount = intent.getDoubleExtra(AMOUNT_KEY, 0.0)
        payMethod = intent.getSerializableExtra(PAY_METHOD) as ModelPaymentMethod
        cardIssuer = intent.getSerializableExtra(CARD_ISSUER) as ModelCardIssuer
        installment = intent.getSerializableExtra(INSTALLMENT) as ModelPayerCost

        setListeners()

        val dataList = mutableListOf<ItemSuccess>().apply {
            add(ItemSuccess(getString(R.string.success_amount), amount.toString()))
            add(ItemSuccess(getString(R.string.success_payment_id), payMethod.id))
            add(ItemSuccess(getString(R.string.success_payment_name), payMethod.name))
            add(ItemSuccess(getString(R.string.success_issuer_id), cardIssuer.id.toString()))
            add(ItemSuccess(getString(R.string.success_issuer_name), cardIssuer.name))
            add(
                ItemSuccess(
                    getString(R.string.success_installments_value),
                    installment.installments.toString()
                )
            )
            if (installment.labels.isNotEmpty()) {
                add(
                    ItemSuccess(
                        getString(R.string.success_installments_label),
                        installment.labels[0]
                    )
                )
            }
            add(ItemSuccess(getString(R.string.success_installments_message), installment.message))
        }

        displaySuccessData(dataList)

        vConfirmBtn.isEnabled = true
    }

    private fun displaySuccessData(data: List<ItemSuccess>) {
        data.forEach { item ->
            val inflater =
                baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val child = inflater.inflate(R.layout.item_success, null).apply {
                vItemSuccessTitle.text = item.title
                vItemSuccessDescription.text = item.description
            }
            vResultDataContainer.addView(child)
        }
    }

    override fun onBackPressed() {
        InstallmentsActivity.start(baseContext, amount, payMethod, cardIssuer)
        finish()
    }

    private fun setListeners() {
        vConfirmBtn.setOnClickListener {
            AmountActivity.start(baseContext)
            finish()
        }
    }

    private data class ItemSuccess(
        val title: String,
        val description: String
    )

    companion object {
        const val AMOUNT_KEY = "amount_value"
        const val PAY_METHOD = "payment_method"
        const val CARD_ISSUER = "card_issuer"
        const val INSTALLMENT = "installment"

        fun start(
            context: Context,
            amount: Double,
            payMethod: ModelPaymentMethod,
            cardIssuer: ModelCardIssuer,
            installment: ModelPayerCost
        ) {
            val starter = Intent(context, SuccessActivity::class.java).apply {
                putExtra(AMOUNT_KEY, amount)
                putExtra(PAY_METHOD, payMethod)
                putExtra(CARD_ISSUER, cardIssuer)
                putExtra(INSTALLMENT, installment)
            }
            context.startActivity(starter)
        }
    }
}