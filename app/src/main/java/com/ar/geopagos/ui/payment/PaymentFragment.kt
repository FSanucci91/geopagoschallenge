package com.ar.geopagos.ui.payment

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseFragment
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.amount.AmountActivity
import com.ar.geopagos.ui.banks.BanksActivity
import com.ar.geopagos.utils.adapter.GenericAdapter
import com.ar.geopagos.utils.adapter.GenericViewHolderFactory
import com.ar.geopagos.utils.adapter.IGenericListener
import kotlinx.android.synthetic.main.fragment_payment_methods.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class PaymentFragment : BaseFragment<PaymentPresenter>(), PaymentView {

    override val presenter: PaymentPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_payment_methods

    private var amount: Double = 0.0
    private var paymentMethodList = mutableListOf<ModelPaymentMethod>()
    private lateinit var listAdapter: GenericAdapter<Any>

    override fun init() {
        amount = arguments!!.getDouble(PaymentActivity.AMOUNT_KEY, 0.0)

        listAdapter = object : GenericAdapter<Any>() {
            override fun getLayoutId(position: Int, obj: Any): Int = when (obj) {
                is ModelPaymentMethod -> R.layout.item_card_paymethod
                is ModelCardIssuer -> R.layout.item_card_bank
                else -> R.layout.item_card_installment
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder =
                GenericViewHolderFactory.create(view, viewType, object : IGenericListener {
                    override fun onItemClick(position: Int) {
                        presenter.onItemClicked(position, paymentMethodList)
                    }
                })
        }

        vRecyclerView.run {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = listAdapter
        }
    }

    override fun setListeners() {
        vErrorButton.setOnClickListener {
            backAction()
        }

        vConfirmBtn.setOnClickListener {
            presenter.onConfirmButtonClicked()
        }
    }

    override fun getAmountValue(): Double = amount

    override fun toggleProgressBar(active: Boolean) {
        vProgressBar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun showPaymentMethodsData(payMethodsList: List<ModelPaymentMethod>) {
        paymentMethodList.apply {
            clear()
            addAll(payMethodsList)
        }
        listAdapter.setItems(payMethodsList)
        toggleRecyclerViewData(true)
    }

    override fun showMaxLimitOverdueError() {
        vSubtitleError.text = getText(R.string.payment_overdue_error)
        toggleRecyclerViewData(false)
    }

    override fun showEmptyDataResponse() {
        vSubtitleError.text = getText(R.string.service_empty_data_error)
        toggleRecyclerViewData(false)
    }

    override fun showDeprecatedError() {
        vSubtitleError.text = getText(R.string.payment_deprecated_error)
        toggleRecyclerViewData(false)
    }

    override fun showServiceError() {
        vSubtitleError.text = getText(R.string.service_service_error)
        toggleRecyclerViewData(false)
    }

    override fun showNetworkError() {
        vSubtitleError.text = getText(R.string.service_network_error)
        toggleRecyclerViewData(false)
    }

    override fun verifyAvailableNetwork(): Boolean {
        val connectivityManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun updateDataList(data: List<ModelPaymentMethod>) {
        listAdapter.setItems(data)
        vConfirmBtn.isEnabled = true
    }

    override fun startBankActivity(payMethod: ModelPaymentMethod, amount: Double) {
        BanksActivity.start(requireContext(), amount, payMethod)
        requireActivity().finish()
    }

    private fun toggleRecyclerViewData(active: Boolean) {
        vRecyclerView.visibility = if (active) View.VISIBLE else View.GONE
        vTitle.visibility = if (active) View.VISIBLE else View.GONE
        vConfirmBtn.visibility = if (active) View.VISIBLE else View.GONE
        vErrorContainer.visibility = if (active) View.GONE else View.VISIBLE
    }

    private fun backAction() {
        AmountActivity.start(requireContext())
        requireActivity().finish()
    }

    companion object {
        fun newInstance(context: Context, arguments: Bundle?) =
            instantiate(context, PaymentFragment::class.java.name, arguments) as PaymentFragment
    }
}