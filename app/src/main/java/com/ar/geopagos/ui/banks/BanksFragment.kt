package com.ar.geopagos.ui.banks

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseFragment
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.installments.InstallmentsActivity
import com.ar.geopagos.ui.payment.PaymentActivity
import com.ar.geopagos.utils.adapter.GenericAdapter
import com.ar.geopagos.utils.adapter.GenericViewHolderFactory
import com.ar.geopagos.utils.adapter.IGenericListener
import kotlinx.android.synthetic.main.fragment_payment_methods.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class BanksFragment : BaseFragment<BanksPresenter>(), BanksView {

    override val presenter: BanksPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_banks

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod
    private var cardIssuersList = mutableListOf<ModelCardIssuer>()
    private lateinit var listAdapter: GenericAdapter<Any>

    override fun init() {
        amount = arguments!!.getDouble(BanksActivity.AMOUNT_KEY, 0.0)
        payMethod = arguments!!.getSerializable(BanksActivity.PAY_METHOD) as ModelPaymentMethod

        listAdapter = object : GenericAdapter<Any>() {
            override fun getLayoutId(position: Int, obj: Any): Int = when (obj) {
                is ModelPaymentMethod -> R.layout.item_card_paymethod
                is ModelCardIssuer -> R.layout.item_card_bank
                else -> R.layout.item_card_installment
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder =
                GenericViewHolderFactory.create(view, viewType, object : IGenericListener {
                    override fun onItemClick(position: Int) {
                        presenter.onItemClicked(position, cardIssuersList)
                    }
                })
        }

        vRecyclerView.run {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = listAdapter
        }
    }

    override fun setListeners() {
        vErrorButton.setOnClickListener {
            backAction()
        }

        vConfirmBtn.setOnClickListener {
            presenter.onConfirmButtonClicked()
        }
    }

    override fun getAmountValue(): Double = amount

    override fun getPayMethod(): ModelPaymentMethod = payMethod

    override fun toggleProgressBar(active: Boolean) {
        vProgressBar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun showCardIssuersData(data: List<ModelCardIssuer>) {
        cardIssuersList.apply {
            clear()
            addAll(data)
        }
        listAdapter.setItems(cardIssuersList)
        toggleRecyclerViewData(true)
    }

    override fun showEmptyDataResponse() {
        vSubtitleError.text = getText(R.string.service_empty_data_error)
        toggleRecyclerViewData(false)
    }

    override fun showServiceError() {
        vSubtitleError.text = getText(R.string.service_service_error)
        toggleRecyclerViewData(false)
    }

    override fun showNetworkError() {
        vSubtitleError.text = getText(R.string.service_network_error)
        toggleRecyclerViewData(false)
    }

    override fun verifyAvailableNetwork(): Boolean {
        val connectivityManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun updateDataList(data: List<ModelCardIssuer>) {
        listAdapter.setItems(cardIssuersList)
        vConfirmBtn.isEnabled = true
    }

    override fun startInstallmentsActivity(
        cardIssuer: ModelCardIssuer,
        payMethod: ModelPaymentMethod,
        amount: Double
    ) {
        InstallmentsActivity.start(requireContext(), amount, payMethod, cardIssuer)
    }

    private fun toggleRecyclerViewData(active: Boolean) {
        vRecyclerView.visibility = if (active) View.VISIBLE else View.GONE
        vTitle.visibility = if (active) View.VISIBLE else View.GONE
        vConfirmBtn.visibility = if (active) View.VISIBLE else View.GONE
        vErrorContainer.visibility = if (active) View.GONE else View.VISIBLE
    }

    private fun backAction() {
        PaymentActivity.start(requireContext(), amount)
        requireActivity().finish()
    }

    companion object {
        fun newInstance(context: Context, arguments: Bundle?) =
            instantiate(context, BanksFragment::class.java.name, arguments) as BanksFragment
    }
}