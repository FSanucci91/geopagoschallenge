package com.ar.geopagos.ui.amount

import android.text.Editable
import android.text.TextWatcher
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseFragment
import com.ar.geopagos.ui.payment.PaymentActivity
import kotlinx.android.synthetic.main.fragment_amount.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class AmountFragment : BaseFragment<AmountPresenter>(), AmountView {

    override val presenter: AmountPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_amount

    override fun init() {
    }

    override fun setListeners() {

        vAmount.setOnClickListener {
            vAmount.setSelection(vAmount.length())
        }

        vAmount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                vAmount.removeTextChangedListener(this)
                presenter.onTextChanged(s.toString(), start, count)
                vAmount.addTextChangedListener(this)
            }
        })

        vConfirmBtn.setOnClickListener {
            presenter.onButtonConfirmClicked()
        }
    }

    override fun setTextWatcherValue(valueFormatted: String) {
        vAmount.run {
            setText(valueFormatted)
            setSelection(valueFormatted.length)
        }
    }

    override fun toggleButtonEnable(active: Boolean) {
        vConfirmBtn.isEnabled = active
    }

    override fun startPaymentActivity(amount: Double) {
        PaymentActivity.start(requireContext(), amount)
        requireActivity().finish()
    }
}
