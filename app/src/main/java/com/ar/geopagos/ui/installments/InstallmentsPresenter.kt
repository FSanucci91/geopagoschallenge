package com.ar.geopagos.ui.installments

import com.ar.geopagos.core.BasePresenter
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelInstallment
import com.ar.geopagos.model.ModelPayerCost
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.installments.IInstallmentsListener
import com.ar.geopagos.network.installments.InstallmentsAdapter

class InstallmentsPresenter(
    override val view: InstallmentsView
) : BasePresenter<InstallmentsView> {

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod
    private lateinit var cardIssuer: ModelCardIssuer
    private lateinit var installmentSelected: ModelPayerCost
    private val serviceResponse = mutableListOf<ModelInstallment>()
    var adapter = InstallmentsAdapter()

    override fun onAttachView() {
        amount = view.getAmountValue()
        payMethod = view.getPayMethod()
        cardIssuer = view.getCardIssuer()

        if (view.verifyAvailableNetwork()) {
            adapter.installmentsService(
                amount.toLong(),
                payMethod.id,
                cardIssuer.id.toLong(),
                object : IInstallmentsListener {
                    override fun onSuccessResponse(data: List<ModelInstallment>) {
                        serviceResponse.addAll(data)
                        view.run {
                            showInstallmentsData(serviceResponse[0].installments)
                            toggleProgressBar(false)
                        }
                    }

                    override fun onEmptyResponse() {
                        view.run {
                            showEmptyDataResponse()
                            toggleProgressBar(false)
                        }
                    }

                    override fun onErrorResponse() {
                        view.run {
                            showServiceError()
                            toggleProgressBar(false)
                        }
                    }
                })
        } else {
            view.showNetworkError()
        }
    }

    fun onItemClicked(position: Int, installments: MutableList<ModelPayerCost>) {
        installments.forEach { item ->
            item.selected = false
        }
        with(installments[position]) {
            selected = true
            installmentSelected = this
        }
        view.updateDataList(installments)
    }

    fun onConfirmButtonClicked() {
        with(view) {
            startConfirmationActivity(installmentSelected, cardIssuer, payMethod, amount)
        }
    }
}