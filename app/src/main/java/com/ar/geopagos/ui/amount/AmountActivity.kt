package com.ar.geopagos.ui.amount

import android.content.Context
import android.content.Intent
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseActivity
import org.koin.android.ext.android.inject

class AmountActivity : BaseActivity() {
    private val fragment: AmountFragment by inject()

    override fun layout(): Int = R.layout.activity_base

    override fun init() = replaceFragment(R.id.vActivityBaseContent, fragment)

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, AmountActivity::class.java)
            context.startActivity(starter)
        }
    }
}
