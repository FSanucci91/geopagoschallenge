package com.ar.geopagos.ui.banks

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseActivity
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.payment.PaymentActivity

class BanksActivity : BaseActivity() {

    private var amount: Double = 0.0

    override fun layout(): Int = R.layout.activity_base

    override fun init() {
        amount = intent.getDoubleExtra(AMOUNT_KEY, 0.0)
        val arguments = Bundle().apply {
            putDouble(AMOUNT_KEY, amount)
            putSerializable(PAY_METHOD, intent.getSerializableExtra(PAY_METHOD))
        }
        val fragment: BanksFragment = BanksFragment.newInstance(baseContext, arguments)
        replaceFragment(R.id.vActivityBaseContent, fragment)
    }

    override fun onBackPressed() {
        PaymentActivity.start(baseContext, amount)
        finish()
    }

    companion object {
        const val AMOUNT_KEY = "amount_value"
        const val PAY_METHOD = "payment_method"

        fun start(context: Context, amount: Double, payMethod: ModelPaymentMethod) {
            val starter = Intent(context, BanksActivity::class.java).apply {
                putExtra(AMOUNT_KEY, amount)
                putExtra(PAY_METHOD, payMethod)
            }
            context.startActivity(starter)
        }
    }
}