package com.ar.geopagos.ui.amount

import com.ar.geopagos.core.BasePresenter

class AmountPresenter(
    override val view: AmountView
) : BasePresenter<AmountView> {

    private var amount = 0.0

    override fun onAttachView() {
        numberFormatConversion()
    }

    fun onTextChanged(text: String, start: Int, status: Int) {
        when (status) {
            OPERATION_SUB -> {
                val amountStr = amount.toInt().toString()
                val charToDelete = amountStr.substring(amountStr.length - 1)
                amount = (amount - charToDelete.toDouble()) / 10
            }
            OPERATION_ADD -> {
                val newChar = text.substring(start).toDouble()
                if (!(amount == 0.0 && newChar == 0.0)) {
                    amount = (amount * 10) + newChar
                }
            }
        }
        numberFormatConversion()
    }

    fun onButtonConfirmClicked() {
        view.startPaymentActivity(amount)
    }

    private fun numberFormatConversion() {
        val newAmount = "\$ " + String.format(ROUND_DECIMALS, (amount / 100))
        view.run {
            setTextWatcherValue(newAmount)
            toggleButtonEnable(amount > 0)
        }
    }

    companion object {
        private const val OPERATION_SUB = 0
        private const val OPERATION_ADD = 1
        private const val ROUND_DECIMALS = "%.2f"
    }
}
