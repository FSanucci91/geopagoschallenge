package com.ar.geopagos.ui.installments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ar.geopagos.R
import com.ar.geopagos.core.BaseActivity
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.ui.banks.BanksActivity

class InstallmentsActivity : BaseActivity() {

    private var amount: Double = 0.0
    private lateinit var payMethod: ModelPaymentMethod

    override fun layout(): Int = R.layout.activity_base

    override fun init() {
        amount = intent.getDoubleExtra(AMOUNT_KEY, 0.0)
        payMethod = intent.getSerializableExtra(PAY_METHOD) as ModelPaymentMethod
        val arguments = Bundle().apply {
            putDouble(AMOUNT_KEY, amount)
            putSerializable(PAY_METHOD, intent.getSerializableExtra(PAY_METHOD))
            putSerializable(CARD_ISSUER, intent.getSerializableExtra(CARD_ISSUER))
        }
        val fragment: InstallmentsFragment =
            InstallmentsFragment.newInstance(baseContext, arguments)
        replaceFragment(R.id.vActivityBaseContent, fragment)
    }

    override fun onBackPressed() {
        BanksActivity.start(baseContext, amount, payMethod)
        finish()
    }

    companion object {
        const val AMOUNT_KEY = "amount_value"
        const val PAY_METHOD = "payment_method"
        const val CARD_ISSUER = "card_issuer"

        fun start(
            context: Context,
            amount: Double,
            payMethod: ModelPaymentMethod,
            cardIssuer: ModelCardIssuer
        ) {
            val starter = Intent(context, InstallmentsActivity::class.java).apply {
                putExtra(AMOUNT_KEY, amount)
                putExtra(PAY_METHOD, payMethod)
                putExtra(CARD_ISSUER, cardIssuer)
            }
            context.startActivity(starter)
        }
    }
}