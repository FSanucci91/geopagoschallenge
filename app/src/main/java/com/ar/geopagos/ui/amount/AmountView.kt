package com.ar.geopagos.ui.amount

interface AmountView {

    fun setTextWatcherValue(valueFormatted: String)

    fun toggleButtonEnable(active: Boolean)

    fun startPaymentActivity(amount: Double)
}