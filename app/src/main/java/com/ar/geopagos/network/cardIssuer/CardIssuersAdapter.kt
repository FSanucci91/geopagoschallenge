package com.ar.geopagos.network.cardIssuer

import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.network.core.RetrofitService
import com.ar.geopagos.utils.Configuration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardIssuersAdapter {

    fun cardIssuersService(payMethodId: String, listener: ICardIssuersListener) {
        RetrofitService()
            .getInstance()
            .create(ICardIssuersService::class.java)
            .getCardIssuers(Configuration.PUBLIC_KEY, payMethodId)
            .enqueue(object : Callback<List<ModelCardIssuer>> {
                override fun onResponse(
                    call: Call<List<ModelCardIssuer>>,
                    response: Response<List<ModelCardIssuer>>
                ) {
                    response.body()?.let {
                        if (it.isEmpty()) {
                            listener.onEmptyResponse()
                        } else {
                            listener.onSuccessResponse(it)
                        }
                    } ?: run {
                        listener.onEmptyResponse()
                    }
                }

                override fun onFailure(call: Call<List<ModelCardIssuer>>, t: Throwable) {
                    listener.onErrorResponse()
                }
            })
    }
}