package com.ar.geopagos.network.core

interface IBaseListener<T> {

    fun onSuccessResponse(data: List<T>)

    fun onEmptyResponse()

    fun onErrorResponse()
}