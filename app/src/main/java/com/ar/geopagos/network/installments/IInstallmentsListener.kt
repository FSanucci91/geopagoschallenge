package com.ar.geopagos.network.installments

import com.ar.geopagos.model.ModelInstallment
import com.ar.geopagos.network.core.IBaseListener

interface IInstallmentsListener : IBaseListener<ModelInstallment>