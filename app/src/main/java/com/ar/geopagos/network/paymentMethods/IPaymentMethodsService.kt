package com.ar.geopagos.network.paymentMethods

import com.ar.geopagos.model.ModelPaymentMethod
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IPaymentMethodsService {

    @GET("payment_methods")
    fun getPaymentMethods(@Query(PUBLIC_KEY) key: String): Call<List<ModelPaymentMethod>>

    companion object {
        private const val PUBLIC_KEY = "public_key"
    }
}