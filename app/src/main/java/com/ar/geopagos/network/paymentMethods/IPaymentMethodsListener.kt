package com.ar.geopagos.network.paymentMethods

import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.core.IBaseListener

interface IPaymentMethodsListener : IBaseListener<ModelPaymentMethod> {
    fun onDeprecatedPayMethodsResponse()
}