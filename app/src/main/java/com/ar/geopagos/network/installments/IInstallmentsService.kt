package com.ar.geopagos.network.installments

import com.ar.geopagos.model.ModelInstallment
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IInstallmentsService {

    @GET("payment_methods/installments")
    fun getInstallments(
        @Query(PUBLIC_KEY) key: String,
        @Query(AMOUNT) amount: Long,
        @Query(PAYMENT_METHOD) payMethodId: String,
        @Query(ISSUER) issuerId: Long
    ): Call<List<ModelInstallment>>

    companion object {
        private const val PUBLIC_KEY = "public_key"
        private const val AMOUNT = "amount"
        private const val PAYMENT_METHOD = "payment_method_id"
        private const val ISSUER = "issuer.id"
    }
}