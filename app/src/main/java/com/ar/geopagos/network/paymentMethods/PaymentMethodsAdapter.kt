package com.ar.geopagos.network.paymentMethods

import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.core.RetrofitService
import com.ar.geopagos.utils.Configuration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentMethodsAdapter {

    fun paymentMethodsService(listener: IPaymentMethodsListener) {
        RetrofitService()
            .getInstance()
            .create(IPaymentMethodsService::class.java)
            .getPaymentMethods(Configuration.PUBLIC_KEY).run {
                enqueue(object : Callback<List<ModelPaymentMethod>> {
                    override fun onResponse(
                        call: Call<List<ModelPaymentMethod>>,
                        response: Response<List<ModelPaymentMethod>>
                    ) {
                        response.body()?.let { paymentMethodList ->
                            paymentMethodList.filter { it.status == ACTIVE }
                            if (paymentMethodList.isNotEmpty()) {
                                listener.onSuccessResponse(paymentMethodList)
                            } else {
                                listener.onDeprecatedPayMethodsResponse()
                            }
                        } ?: run {
                            listener.onEmptyResponse()
                        }
                    }

                    override fun onFailure(call: Call<List<ModelPaymentMethod>>, t: Throwable) {
                        listener.onErrorResponse()
                    }
                })
            }
    }

    companion object {
        private const val ACTIVE = "active"
    }
}