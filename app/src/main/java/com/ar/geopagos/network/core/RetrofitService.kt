package com.ar.geopagos.network.core

import com.ar.geopagos.utils.Configuration
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService constructor(
    private val mRetrofit: Retrofit = Retrofit.Builder()
        .baseUrl(Configuration.API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
) {

    fun getInstance(): Retrofit = mRetrofit
}