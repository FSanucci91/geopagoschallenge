package com.ar.geopagos.network.cardIssuer

import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.network.core.IBaseListener

interface ICardIssuersListener : IBaseListener<ModelCardIssuer>