package com.ar.geopagos.network.cardIssuer

import com.ar.geopagos.model.ModelCardIssuer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ICardIssuersService {
    @GET("payment_methods/card_issuers")
    fun getCardIssuers(
        @Query(PUBLIC_KEY) key: String,
        @Query(PAYMENT_METHOD) payMethodId: String
    ): Call<List<ModelCardIssuer>>

    companion object {
        private const val PUBLIC_KEY = "public_key"
        private const val PAYMENT_METHOD = "payment_method_id"
    }
}