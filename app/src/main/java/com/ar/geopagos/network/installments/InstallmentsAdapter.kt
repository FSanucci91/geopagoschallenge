package com.ar.geopagos.network.installments

import com.ar.geopagos.model.ModelInstallment
import com.ar.geopagos.network.core.RetrofitService
import com.ar.geopagos.utils.Configuration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InstallmentsAdapter {

    fun installmentsService(
        amount: Long,
        payMethodId: String,
        issuerId: Long,
        listener: IInstallmentsListener
    ) {
        RetrofitService()
            .getInstance()
            .create(IInstallmentsService::class.java)
            .getInstallments(Configuration.PUBLIC_KEY, amount, payMethodId, issuerId)
            .enqueue(object : Callback<List<ModelInstallment>> {
                override fun onResponse(
                    call: Call<List<ModelInstallment>>,
                    response: Response<List<ModelInstallment>>
                ) {
                    response.body()?.let {
                        if (it.isEmpty()) {
                            listener.onEmptyResponse()
                        } else {
                            listener.onSuccessResponse(it)
                        }
                    } ?: run {
                        listener.onEmptyResponse()
                    }
                }

                override fun onFailure(call: Call<List<ModelInstallment>>, t: Throwable) {
                    listener.onErrorResponse()
                }
            })
    }
}