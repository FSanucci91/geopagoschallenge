package com.ar.geopagos.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelInstallment(
    @SerializedName("payment_method_id")
    val payMethodId: String,
    @SerializedName("payment_type_id")
    val payMethodType: String,
    @SerializedName("issuer")
    val issuer: ModelIssuer,
    @SerializedName("payer_costs")
    val installments: List<ModelPayerCost>
) : Serializable