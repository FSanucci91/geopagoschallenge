package com.ar.geopagos.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelPayerCost(
    @SerializedName("installments")
    val installments: Int,
    @SerializedName("labels")
    val labels: List<String>,
    @SerializedName("recommended_message")
    val message: String,
    @SerializedName("installment_amount")
    val installmentAmount: Float,
    @SerializedName("total_amount")
    val totalAmount: Float,
    var selected: Boolean
) : Serializable