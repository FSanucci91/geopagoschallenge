package com.ar.geopagos.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelPaymentMethod(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("thumbnail")
    val thumbnail: String,
    @SerializedName("max_allowed_amount")
    val maxValue: Long,
    var selected: Boolean
) : Serializable