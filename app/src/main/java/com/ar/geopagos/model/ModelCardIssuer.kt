package com.ar.geopagos.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelCardIssuer(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("thumbnail")
    val thumbnail: String,
    var selected: Boolean
) : Serializable