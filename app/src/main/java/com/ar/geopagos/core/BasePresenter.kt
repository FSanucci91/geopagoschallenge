package com.ar.geopagos.core

interface BasePresenter<T> {
    val view: T

    fun onAttachView()
}
