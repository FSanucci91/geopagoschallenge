package com.ar.geopagos.utils.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ar.geopagos.R
import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPayerCost
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.utils.loadImage
import kotlinx.android.synthetic.main.item_card_installment.view.*
import kotlinx.android.synthetic.main.item_card_paymethod.view.*
import kotlinx.android.synthetic.main.item_card_bank.view.*

object GenericViewHolderFactory {

    fun create(view: View, viewType: Int, listener: IGenericListener): RecyclerView.ViewHolder =
        when (viewType) {
            R.layout.item_card_paymethod -> PayMethodViewHolder(view, listener)
            R.layout.item_card_bank -> BankViewHolder(view, listener)
            else -> {
                InstallmentViewHolder(view, listener)
            }
        }

    class PayMethodViewHolder(itemView: View, private val listener: IGenericListener) :
        RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<ModelPaymentMethod> {

        override fun bind(data: ModelPaymentMethod) {
            itemView.run {
                vPaymentCardName.text = data.name
                vPaymentRadioButton.run {
                    isChecked = data.selected
                    setOnClickListener {
                        listener.onItemClick(position)
                    }
                }
                vPaymentItemContainer.setOnClickListener {
                    listener.onItemClick(position)
                }
                vPaymentCardImage.loadImage(data.thumbnail)
            }
        }
    }

    class BankViewHolder(itemView: View, private val listener: IGenericListener) :
        RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<ModelCardIssuer> {

        override fun bind(data: ModelCardIssuer) {
            itemView.run {
                vBankCardName.text = data.name
                vBankRadioButton.run {
                    isChecked = data.selected
                    setOnClickListener {
                        listener.onItemClick(position)
                    }
                }
                vBankItemContainer.setOnClickListener {
                    listener.onItemClick(position)
                }
                vBankCardImage.loadImage(data.thumbnail)
            }
        }
    }

    class InstallmentViewHolder(itemView: View, private val listener: IGenericListener) :
        RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<ModelPayerCost> {

        override fun bind(data: ModelPayerCost) {
            itemView.run {
                vInstallmentRadioButton.run {
                    isChecked = data.selected
                    setOnClickListener {
                        listener.onItemClick(position)
                    }
                }

                vInstallmentInstallments.text =
                    context.getString(R.string.installments_number, data.installments)

                if (data.labels.isEmpty()) {
                    vInstallmentCost.visibility = View.GONE
                } else {
                    vInstallmentCost.text =
                        context.getString(R.string.installments_label, data.labels[0])
                }

                vInstallmentTotalValue.text =
                    context.getString(R.string.installments_total, data.message)

                vInstallmentItemContainer.setOnClickListener {
                    listener.onItemClick(position)
                }
            }
        }
    }
}