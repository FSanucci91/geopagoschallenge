package com.ar.geopagos.utils.koin

import com.ar.geopagos.ui.amount.AmountFragment
import com.ar.geopagos.ui.amount.AmountPresenter
import com.ar.geopagos.ui.amount.AmountView
import com.ar.geopagos.ui.banks.BanksFragment
import com.ar.geopagos.ui.banks.BanksPresenter
import com.ar.geopagos.ui.banks.BanksView
import com.ar.geopagos.ui.installments.InstallmentsFragment
import com.ar.geopagos.ui.installments.InstallmentsPresenter
import com.ar.geopagos.ui.installments.InstallmentsView
import com.ar.geopagos.ui.payment.PaymentFragment
import com.ar.geopagos.ui.payment.PaymentPresenter
import com.ar.geopagos.ui.payment.PaymentView
import org.koin.core.qualifier.named
import org.koin.dsl.module

val koinModules = module {

    // 1. Amount Input View
    factory { AmountFragment() }
    scope(named<AmountFragment>()) {
        scoped { (view: AmountView) -> AmountPresenter(view) }
    }

    // 2. Payment Methods List
    factory { PaymentFragment() }
    scope(named<PaymentFragment>()) {
        scoped { (view: PaymentView) -> PaymentPresenter(view) }
    }

    // 3. Banks List
    factory { BanksFragment() }
    scope(named<BanksFragment>()) {
        scoped { (view: BanksView) -> BanksPresenter(view) }
    }

    // 4. Installments List
    factory { InstallmentsFragment() }
    scope(named<InstallmentsFragment>()) {
        scoped { (view: InstallmentsView) -> InstallmentsPresenter(view) }
    }
}
