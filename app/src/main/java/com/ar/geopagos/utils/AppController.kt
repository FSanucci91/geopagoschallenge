package com.ar.geopagos.utils

import com.ar.geopagos.core.BaseApplication
import com.ar.geopagos.utils.RecipeModules.initializeModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppController : BaseApplication() {

    override fun onInit() {

        startKoin {
            androidContext(this@AppController)
            initializeModules()
        }
    }
}
