package com.ar.geopagos.utils.adapter

interface IGenericListener {
    fun onItemClick(position: Int)
}