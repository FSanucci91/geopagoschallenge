package com.ar.geopagos.utils

import com.ar.geopagos.utils.koin.ToastFactory
import com.ar.geopagos.utils.koin.koinModules
import org.koin.core.KoinApplication
import org.koin.core.module.Module
import org.koin.dsl.module

object RecipeModules {

    /** Injections for all the application. */
    private val appModule: Module = module {
        /** A single instance will be created for all injection. */
        single { ToastFactory(get()) }
    }

    fun KoinApplication.initializeModules() = modules(
        listOf(
            appModule,
            koinModules
        )
    )
}
