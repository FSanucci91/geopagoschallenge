package com.ar.geopagos.utils

class Configuration {
    /** CONFIGURATION FILE**/
    companion object {
        const val API_URL = "https://api.mercadopago.com/v1/"
        const val PUBLIC_KEY = "TEST-43317d64-28a9-4ebb-be08-09a12b03b3d9"
    }
}
