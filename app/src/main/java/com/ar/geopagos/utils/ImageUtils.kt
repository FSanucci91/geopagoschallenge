package com.ar.geopagos.utils

import android.widget.ImageView
import com.ar.geopagos.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImage(uri: String?) {
    val options = RequestOptions()
        .placeholder(R.drawable.ic_no_image)
        .fitCenter()
        .error(R.drawable.ic_no_image)
    Glide.with(this.context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .into(this)
}