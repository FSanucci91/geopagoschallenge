package com.ar.geopagos.payment

import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.paymentMethods.IPaymentMethodsListener
import com.ar.geopagos.network.paymentMethods.PaymentMethodsAdapter
import com.ar.geopagos.ui.payment.PaymentPresenter
import com.ar.geopagos.ui.payment.PaymentView
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

/**
 * PaymentMethodsFragmentPresenterTest local unit test, which will execute on the development machine (host).
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PaymentMethodsFragmentPresenterTest {

    private lateinit var view: PaymentView
    private lateinit var presenter: PaymentPresenter
    private lateinit var mockAdapter: PaymentMethodsAdapter

    @Test
    fun `given network error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn false
        }
        presenter = PaymentPresenter(view)

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showNetworkError()
    }

    @Test
    fun `given service error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
        }
        mockAdapter = mock {
            on { paymentMethodsService(any()) } doAnswer {
                it.getArgument<IPaymentMethodsListener>(0).onErrorResponse()
                mock()
            }
        }
        presenter = PaymentPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showServiceError()
    }

    @Test
    fun `given deprecated error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
        }
        mockAdapter = mock {
            on { paymentMethodsService(any()) } doAnswer {
                it.getArgument<IPaymentMethodsListener>(0).onDeprecatedPayMethodsResponse()
                mock()
            }
        }
        presenter = PaymentPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showDeprecatedError()
    }

    @Test
    fun `given empty data error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
        }
        mockAdapter = mock {
            on { paymentMethodsService(any()) } doAnswer {
                it.getArgument<IPaymentMethodsListener>(0).onEmptyResponse()
                mock()
            }
        }
        presenter = PaymentPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showEmptyDataResponse()
    }

    @Test
    fun `given overdue error when service is call then hide recycler view data`() {
        // GIVE
        val data = mutableListOf<ModelPaymentMethod>().apply { add(generatePaymentMethod(200.0)) }
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 1000.0
        }
        mockAdapter = mock {
            on { paymentMethodsService(any()) } doAnswer {
                it.getArgument<IPaymentMethodsListener>(0).onSuccessResponse(data)
                mock()
            }
        }
        presenter = PaymentPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showMaxLimitOverdueError()
    }

    @Test
    fun `given success response when service is call then show recycler view data`() {
        // GIVE
        val data = mutableListOf<ModelPaymentMethod>().apply { add(generatePaymentMethod(2000.0)) }
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
        }
        mockAdapter = mock {
            on { paymentMethodsService(any()) } doAnswer {
                it.getArgument<IPaymentMethodsListener>(0).onSuccessResponse(data)
                mock()
            }
        }
        presenter = PaymentPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showPaymentMethodsData(eq(data))
    }

    private fun generatePaymentMethod(limit: Double): ModelPaymentMethod = ModelPaymentMethod(
        ID, NAME, STATUS, IMAGE, limit.toLong(), SELECTED
    )

    companion object {
        private const val ID = "visa_id"
        private const val NAME = "VISA"
        private const val STATUS = "ACTIVE"
        private const val IMAGE = ""
        private const val SELECTED = false
    }
}