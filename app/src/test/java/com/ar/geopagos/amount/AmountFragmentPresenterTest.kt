package com.ar.geopagos.amount

import com.ar.geopagos.ui.amount.AmountPresenter
import com.ar.geopagos.ui.amount.AmountView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test

/**
 * AmountFragmentPresenterTest local unit test, which will execute on the development machine (host).
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AmountFragmentPresenterTest {

    private lateinit var view: AmountView
    private lateinit var presenter: AmountPresenter

    @Before
    fun createInstances() {
        view = mock()
        presenter = AmountPresenter(view)
    }

    @Test
    fun `given disable button when activity starts`() {
        presenter.onAttachView()
        verify(view, times(1)).toggleButtonEnable(false)
    }

    @Test
    fun `given enable button when amount value is greater than zero`() {
        presenter.onTextChanged("1", 0, 1)
        verify(view, times(1)).toggleButtonEnable(true)
    }

    @Test
    fun `given disable button when amount value is downgrade than zero`() {
        presenter.run {
            onTextChanged("1", 0, 1)
            onTextChanged("", 0, 0)
            verify(view, times(1)).toggleButtonEnable(false)
        }
    }
}