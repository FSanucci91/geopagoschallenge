package com.ar.geopagos.installments

import com.ar.geopagos.model.*
import com.ar.geopagos.network.installments.IInstallmentsListener
import com.ar.geopagos.network.installments.InstallmentsAdapter
import com.ar.geopagos.ui.installments.InstallmentsPresenter
import com.ar.geopagos.ui.installments.InstallmentsView
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

/**
 * InstallmentsFragmentPresenterTest local unit test, which will execute on the development machine (host).
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class InstallmentsFragmentPresenterTest {

    private lateinit var view: InstallmentsView
    private lateinit var presenter: InstallmentsPresenter
    private lateinit var mockAdapter: InstallmentsAdapter

    @Test
    fun `given network error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn false
        }
        presenter = InstallmentsPresenter(view)

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showNetworkError()
    }

    @Test
    fun `given service error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
            on { getCardIssuer() } doReturn generateCardIssue()
        }
        mockAdapter = mock {
            on { installmentsService(any(), any(), any(), any()) } doAnswer {
                it.getArgument<IInstallmentsListener>(3).onErrorResponse()
                mock()
            }
        }
        presenter = InstallmentsPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showServiceError()
    }

    @Test
    fun `given empty data error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
            on { getCardIssuer() } doReturn generateCardIssue()
        }
        mockAdapter = mock {
            on { installmentsService(any(), any(), any(), any()) } doAnswer {
                it.getArgument<IInstallmentsListener>(3).onEmptyResponse()
                mock()
            }
        }
        presenter = InstallmentsPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showEmptyDataResponse()
    }

    @Test
    fun `given success response when service is call then show recycler view data`() {
        // GIVE
        val data = mutableListOf<ModelInstallment>().apply { add(generateInstallments()) }
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
            on { getCardIssuer() } doReturn generateCardIssue()
        }
        mockAdapter = mock {
            on { installmentsService(any(), any(), any(), any()) } doAnswer {
                it.getArgument<IInstallmentsListener>(3).onSuccessResponse(data)
                mock()
            }
        }
        presenter = InstallmentsPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showInstallmentsData(eq(data[0].installments))
    }

    private fun generatePaymentMethod(): ModelPaymentMethod = ModelPaymentMethod(
        PAY_METHOD_ID, PAY_METHOD_ID, STATUS, IMAGE_URL, PAY_METHOD_AMOUNT, SELECTED
    )

    private fun generateCardIssue(): ModelCardIssuer =
        ModelCardIssuer(CARD_ISSUER_ID, IMAGE_URL, CARD_ISSUER_NAME, SELECTED)

    private fun generateInstallments(): ModelInstallment = ModelInstallment(
        PAY_METHOD_ID, PAY_METHOD_ID, ModelIssuer(
            CARD_ISSUER_ID, CARD_ISSUER_NAME
        ), generatePayerCost()
    )

    private fun generatePayerCost(): List<ModelPayerCost> = mutableListOf<ModelPayerCost>().apply {
        add(
            ModelPayerCost(
                PAYER_COST_INSTALLMENT, listOf(),
                PAYER_COST_MESSAGE,
                PAYER_COST_VALUE,
                PAYER_COST_VALUE,
                SELECTED
            )
        )
    }

    companion object {
        private const val PAY_METHOD_ID = "visa"
        private const val STATUS = "ACTIVE"
        private const val PAY_METHOD_AMOUNT = 100L
        private const val CARD_ISSUER_ID = 10
        private const val CARD_ISSUER_NAME = "card_issuer"
        private const val IMAGE_URL = ""
        private const val SELECTED = true
        private const val PAYER_COST_INSTALLMENT = 10
        private const val PAYER_COST_MESSAGE = "message"
        private const val PAYER_COST_VALUE = 10F
    }
}