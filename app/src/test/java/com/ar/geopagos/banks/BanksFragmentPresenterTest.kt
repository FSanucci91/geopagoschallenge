package com.ar.geopagos.banks

import com.ar.geopagos.model.ModelCardIssuer
import com.ar.geopagos.model.ModelPaymentMethod
import com.ar.geopagos.network.cardIssuer.CardIssuersAdapter
import com.ar.geopagos.network.cardIssuer.ICardIssuersListener
import com.ar.geopagos.ui.banks.BanksPresenter
import com.ar.geopagos.ui.banks.BanksView
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

/**
 * BanksFragmentPresenterTest local unit test, which will execute on the development machine (host).
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class BanksFragmentPresenterTest {

    private lateinit var view: BanksView
    private lateinit var presenter: BanksPresenter
    private lateinit var mockAdapter: CardIssuersAdapter

    @Test
    fun `given network error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn false
        }
        presenter = BanksPresenter(view)

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showNetworkError()
    }

    @Test
    fun `given service error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
        }
        mockAdapter = mock {
            on { cardIssuersService(any(), any()) } doAnswer {
                it.getArgument<ICardIssuersListener>(1).onErrorResponse()
                mock()
            }
        }
        presenter = BanksPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showServiceError()
    }

    @Test
    fun `given empty data error when service is call then hide recycler view data`() {
        // GIVE
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
        }
        mockAdapter = mock {
            on { cardIssuersService(any(), any()) } doAnswer {
                it.getArgument<ICardIssuersListener>(1).onEmptyResponse()
                mock()
            }
        }
        presenter = BanksPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showEmptyDataResponse()
    }

    @Test
    fun `given success response when service is call then show recycler view data`() {
        // GIVE
        val data = mutableListOf<ModelCardIssuer>().apply { add(generateCardIssue()) }
        view = mock {
            on { verifyAvailableNetwork() } doReturn true
            on { getAmountValue() } doReturn 100.0
            on { getPayMethod() } doReturn generatePaymentMethod()
        }
        mockAdapter = mock {
            on { cardIssuersService(any(), any()) } doAnswer {
                it.getArgument<ICardIssuersListener>(1).onSuccessResponse(data)
                mock()
            }
        }
        presenter = BanksPresenter(view).apply {
            adapter = mockAdapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showCardIssuersData(eq(data))
    }

    private fun generateCardIssue(): ModelCardIssuer =
        ModelCardIssuer(CARD_ISSUER_ID, IMAGE_URL, CARD_ISSUER_NAME, SELECTED)

    private fun generatePaymentMethod(): ModelPaymentMethod = ModelPaymentMethod(
        PAY_METHOD_ID, PAY_METHOD_ID, STATUS, IMAGE_URL, PAY_METHOD_AMOUNT, SELECTED
    )

    companion object {
        private const val PAY_METHOD_ID = "visa"
        private const val STATUS = "ACTIVE"
        private const val PAY_METHOD_AMOUNT = 100L
        private const val CARD_ISSUER_ID = 10
        private const val CARD_ISSUER_NAME = "card_issuer"
        private const val IMAGE_URL = ""
        private const val SELECTED = true
    }
}